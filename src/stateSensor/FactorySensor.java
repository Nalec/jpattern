package stateSensor;

import java.util.Observable;


import eu.telecomnancy.sensor.AdapterLegacyTempSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class FactorySensor extends Observable implements ISensor{

	String type;
	ISensor sensor;
	
	public FactorySensor(String type) {
		this.type = type;
		
		switch (type) {
		case "StateSensor":
			sensor = new StateSensor();
			break;
		case "StateDecoratorSensor":
			sensor = new StateDecoratorSensor();
			break;
		case "ProxyStateSensor":
			sensor = new ProxyStateSensor(new TemperatureSensor());
			break;
		case "LegacyTemperatureSensor":
			sensor = (ISensor) new AdapterLegacyTempSensor(); // A refaire
		}
	}

	@Override
	public void on() {
		sensor.on();
	}

	@Override
	public void off() {
		sensor.off();
		
	}

	@Override
	public boolean getStatus() {
		sensor.getStatus();
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		sensor.getValue();
		return 0;
	}
}
