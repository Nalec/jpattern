package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.ui.ConsoleUI;


public class AppFactory {
    public static void main(String[] args) {
        ISensor sensor = SensorFactory.makeSensor();
        new ConsoleUI(sensor);
    }
}


// Trouver � quoi �a sert