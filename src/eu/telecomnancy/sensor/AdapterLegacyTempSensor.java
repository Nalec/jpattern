package eu.telecomnancy.sensor;

import java.util.Random;

public class AdapterLegacyTempSensor {
	
	private LegacyTemperatureSensor OldSensor = new LegacyTemperatureSensor();
	private boolean state = false;
	private double value = 0;
	
	public void AdapterLegacyTempSensor(){
		
	
	}
    /**
     * Enable the sensor.
     */
    public void on(){
    	OldSensor.onOff();
    }

    /**
     * Disable the sensor.
     */
    public void off(){
    	OldSensor.onOff();
    }

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus(){
    	state = OldSensor.getStatus();
		return state;
    	
    }

    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public void update() throws SensorNotActivatedException{
    	//AcquiringThread
    }

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException{
    	value = OldSensor.getTemperature();
		return value;
    	
    }

}
