package eu.telecomnancy.sensor;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:07
 */
public class SensorProxy implements ISensor {
    private ISensor sensor;


    public SensorProxy(ISensor _sensor) {
        sensor = _sensor;
        System.out.println("J'arrive dans SensorProxy");
    }

    @Override
    public void on() {
        System.out.println("Sensor On");
        sensor.on();
    }

    @Override
    public void off() {
    	System.out.println("Sensor Off");
        sensor.off();
    }

    @Override
    public boolean getStatus() {
    	System.out.println("Sensor On : sensor.getStatus()");
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	System.out.println("Sensor updated");
        sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	System.out.println("Sensor value :" + sensor.getValue());
        return sensor.getValue();
    }
}
